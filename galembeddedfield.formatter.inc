<?php

/**
 * @file
 * Theming and formatting functions for Embedded Field Input Method
 */         
function theme_galembeddedfield_emvideo($element) {
  return theme('galembeddedfield_common', $element, 'emvideo');
}

function theme_galembeddedfield_emimage($element) {
  return theme('galembeddedfield_common', $element, 'emimage');
}

function theme_galembeddedfield_common($element, $emtype = '') {
  $output = '';
  static $gallery_cache;
  
  foreach (galleryapi_preset_load() as $preset) {
    if ('galleryapi_cckembeddedfield_'. $emtype .'_'. $preset['name'] === $element['#formatter']) {
      $pid = $preset['pid'];
      break;
    }
  }
  
  $gallery_id = 'node-'. $element['#node']->nid .'-'. $element['#field_name'];
  if ($pid && is_numeric($pid)) {
    if (!isset($gallery_cache[$gallery_id])) {
      $data = array();
      foreach ($element as $key => $item) {      
        if (is_numeric($key) && $item['#item']['provider'] && $item['#item']['value']) {
          $slide_id = $gallery_id .'-'. $key;
          $data[$slide_id] = TRUE;
        }        
      } 

      $gallery_cache[$gallery_id] = galleryapi_build_gallery($pid, $data);
      $output .= $gallery_cache[$gallery_id];
    }
    else {
      $output .= $gallery_cache[$gallery_id];
    }    
  }

  return $output;
}